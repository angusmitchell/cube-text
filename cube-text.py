# -*- coding: utf-8 -*-
import MySQLdb
import pylab
import numpy as np
import scipy.stats as stats
import pandas as pd
import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plot
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from sklearn.neighbors import NearestNeighbors
import statsmodels.api as sm
import time
from matplotlib.ticker import FuncFormatter

def cleanDataFrame(df):
    return df.dropna(subset=['avg_measure_depth', 'avg_vert_depth', 'avg_time_to_half_eur',
        'avg_proppant_lbs', 'avg_water_vol_kgal', 'avg_eur', 'avg_cum_prod', 'avg_bh_spacing', 'avg_surf_spacing',
        'well_count', 'avg_perf_length', 'avg_ppg'])#, 'ppf'])

def groupByOperator(df):
	wm = lambda x: np.average(x, weights=df.loc[x.index, 'well_count'])
	return df.groupby('operator_id').agg({'well_count': np.sum, 'avg_time_to_half_eur': np.mean, 
		'avg_measure_depth':wm, 'avg_vert_depth':wm, 'avg_proppant_lbs':wm,
		'avg_water_vol_kgal':wm,'avg_eur':wm, 'avg_cum_prod':wm, 'avg_prod_3':wm,
		'avg_prod_6':wm, 'avg_bh_spacing':wm,'avg_surf_spacing':wm, 'avg_ppg':wm, 'avg_perf_length':wm})#, 'ppf':wm})

#****************************
#     Plot Formatting
#****************************
def setLabels(ax, xlab, ylab, zlab, title, x_thousands = False, y_thousands = False, z_thousands = False):
	ax.set_xlabel(xlab)
	ax.set_ylabel(ylab)
	if(zlab is not None):
		ax.set_zlabel(zlab)
	ax.set_title(title)
	
	fmt = FuncFormatter(formatThousands)
	if(x_thousands):
		ax.xaxis.set_major_formatter(fmt)
	if(y_thousands):
		ax.yaxis.set_major_formatter(fmt)
	if(z_thousands):
		ax.zaxis.set_major_formatter(fmt)

def setAxesLimits(ax, xlim, ylim, zlim, invertZ):
	if(xlim is not None):
		ax.set_xlim(xlim)
	if(ylim is not None):
		ax.set_ylim(ylim)
	if(zlim is not None):
		ax.set_zlim(zlim)
	if(invertZ):
		ax.set_zlim(ax.get_zlim()[::-1])

def getInsightColorScale(scoreRanks, scoreTypes, type):
	df = pd.concat([scoreRanks, scoreTypes], axis=1)
	df['type'] = type
	return df.apply(getInsightRankPlotColor, axis=1), df.apply(getInsightRankPlotBorderColor, axis=1)

def getInsightAlphaScale(scoreRanks, scoreTypes, type):
	df = pd.concat([scoreRanks, scoreTypes], axis=1)
	df['type'] = type
	alphaScores = lambda x: 0.85 if x[0] <= 5 and x[1] == x[2] else 0.1
	return df.apply(alphaScores, axis=1)
	
def getInsightRankPlotBorderColor(row):
	x = row[0]
	y = row[1]
	z = row[2]
	if(y == z):
		return getInsightRankBorderColor(x)
	else:
		return "#D8D8D8"
		
def getInsightRankBorderColor(x):
	if(x == 1):
		return "DarkRed"
	elif(x == 2):
		return "Navy"
	elif(x == 3):
		return "DarkGreen"
	elif(x == 4):
		return "Peru"
	elif(x == 5):
		return "DarkViolet"
	else:
		return "#D8D8D8"
		
def getInsightRankPlotColor(row):
	x = row[0]
	y = row[1]
	z = row[2]
	if(y == z):	
		return getInsightRankColor(x)
	else:
		return "#D8D8D8"			

def getInsightRankColor(x):
	if(x == 1):
		return "Red"
	elif(x == 2):
		return "Blue"
	elif(x == 3):
		return "Green"
	elif(x == 4):
		return "DarkOrange"
	elif(x == 5):
		return "Purple"
	else:
		return "#E8E8E8"

def getNormalizedRGColor(x):
	if(x >= 0):
		if(x < 0.05):
			return "#f2f2f2"
		elif(x < 0.25):
			return "#ccffcc"
		elif(x < 0.45):
			return "#99ff99"
		elif(x < 0.65):
			return "#66ff66"
		elif(x < 0.85):
			return "#1aff1a"
		else:
			return "#00cc00"
	else:
		if(x > -0.05):
			return "#f2f2f2"
		elif(x > -0.25):
			return "#ffcccc"
		elif(x > -0.45):
			return "#ff9999"
		elif(x > -0.65):
			return "#ff6666"
		elif(x > -0.85):
			return "#ff3333"
		else:
			return "#ff0000"

def getNormalizedRGBorderColor(x):
	if(x >= 0):
		if(x < 0.05):
			return "#bfbfbf"
		elif(x < 0.25):
			return "#80ff80"
		elif(x < 0.45):
			return "#66ff66"
		elif(x < 0.65):
			return "#33ff33"
		elif(x < 0.85):
			return "#00e600"
		else:
			return "#006600"
	else:
		if(x > -0.05):
			return "#bfbfbf"
		elif(x > -0.25):
			return "#ffb3b3"
		elif(x > -0.45):
			return "#ff6666"
		elif(x > -0.65):
			return "#ff3333"
		elif(x > -0.85):
			return "#e60000"
		else:
			return "#b30000"
			
def getOutlierColorScale(lof, lofMin, lofMax):
	colormap = plot.get_cmap('Blues')
	cNorm = mpl.colors.Normalize(vmin=lofMin, vmax=lofMax)
	scalarMap = cm.ScalarMappable(norm=cNorm, cmap=colormap)
	color_out = scalarMap.to_rgba(lof)
	borderColor_out = scalarMap.to_rgba(lof + 3)
	return color_out, borderColor_out

def getGreenColorScale(values, min, max):
	colormap = plot.get_cmap('Greens')
	cNorm = mpl.colors.Normalize(vmin=min, vmax=max)
	scalarMap = cm.ScalarMappable(norm=cNorm, cmap=colormap)
	color_out = scalarMap.to_rgba(values)
	borderColor_out = scalarMap.to_rgba(values + ((max - min) / 20))
	return color_out, borderColor_out	

def getAlphaScale(values, min, max):
	return pd.Series((values - min + 0.05) / (max * 1.111))
	
def getNormalizedRGColorScale(values):
	maxValue = values.abs().max()
	minValue = values.abs().min()
	normalizedValues = (values.abs() - minValue) / maxValue * (values / values.abs())
	color = map(lambda x: getNormalizedRGColor(x), normalizedValues) 
	borderColor = map(lambda x: getNormalizedRGBorderColor(x), normalizedValues)
	size = values.abs() / maxValue * 500
	return color, borderColor, size

def getColumnNorm(x):
	return (x - x.mean()) / x.std()

def suppressMovementColors(color, borderColor, x, y, z, variable, sign, moveBasedOnSd, moveSd):
	x_norm = getColumnNorm(x)
	y_norm = getColumnNorm(y)
	z_norm = getColumnNorm(z)
	color_suppressed = np.array(color, copy = True)
	borderColor_suppressed = np.array(borderColor, copy = True)
	alpha = np.full(len(color), 0.85)
	alpha_suppressed = pd.Series(alpha)
	#print color_suppressed
	for i, (c, bc) in enumerate(zip(color_suppressed, borderColor_suppressed)):
		if variable == "x":
			if sign == "positive":
				if ((not moveBasedOnSd and (x_norm.iloc[i] < 0 or x_norm.abs().iloc[i] < y_norm.abs().iloc[i]
						or x_norm.abs().iloc[i] < z_norm.abs().iloc[i]))
					or (moveBasedOnSd and (x_norm.iloc[i] < 0 or x_norm.abs().iloc[i] < moveSd))):
					color_suppressed[i] = "#f2f2f2"
					borderColor_suppressed[i] = "#bfbfbf"
					alpha_suppressed[i] = 0.1
			else:
				if ((not moveBasedOnSd and (x_norm.iloc[i] >= 0 or x_norm.abs().iloc[i] < y_norm.abs().iloc[i] 
						or x_norm.abs().iloc[i] < z_norm.abs().iloc[i]))
					or (moveBasedOnSd and (x_norm.iloc[i] >= 0 or x_norm.abs().iloc[i] < moveSd))):
					color_suppressed[i] = "#f2f2f2"
					borderColor_suppressed[i] = "#bfbfbf"	
					alpha_suppressed[i] = 0.1					
		elif variable == "y":
			if sign == "positive":
				if ((not moveBasedOnSd and (y_norm.iloc[i] < 0 or y_norm.abs().iloc[i] < x_norm.abs().iloc[i] 
						or y_norm.abs().iloc[i] < z_norm.abs().iloc[i]))
					or (moveBasedOnSd and (y_norm.iloc[i] < 0 or y_norm.abs().iloc[i] < moveSd))):
					color_suppressed[i] = "#f2f2f2"
					borderColor_suppressed[i] = "#bfbfbf"
					alpha_suppressed[i] = 0.1
			else:
				if ((not moveBasedOnSd and (y_norm.iloc[i] >= 0 or y_norm.abs().iloc[i] < x_norm.abs().iloc[i] 
						or y_norm.abs().iloc[i] < z_norm.abs().iloc[i]))
					or (moveBasedOnSd and (y_norm.iloc[i] >= 0 or y_norm.abs().iloc[i] < moveSd))):
					color_suppressed[i] = "#f2f2f2"
					borderColor_suppressed[i] = "#bfbfbf"
					alpha_suppressed[i] = 0.1
		elif variable == "z":
			if sign == "positive":
				if ((not moveBasedOnSd and (z_norm.iloc[i] < 0 or z_norm.abs().iloc[i] < y_norm.abs().iloc[i] 
						or z_norm.abs().iloc[i] < x_norm.abs().iloc[i]))
					or (moveBasedOnSd and (z_norm.iloc[i] < 0 or z_norm.abs().iloc[i] < moveSd))):
					color_suppressed[i] = "#f2f2f2"
					borderColor_suppressed[i] = "#bfbfbf"
					alpha_suppressed[i] = 0.1
			else:
				if ((not moveBasedOnSd and (z_norm.iloc[i] >= 0 or z_norm.abs().iloc[i] < y_norm.abs().iloc[i] 
						or z_norm.abs().iloc[i] < x_norm.abs().iloc[i]))
					or (moveBasedOnSd and (z_norm.iloc[i] >= 0 or z_norm.abs().iloc[i] < moveSd))):
					color_suppressed[i] = "#f2f2f2"
					borderColor_suppressed[i] = "#bfbfbf"	
					alpha_suppressed[i] = 0.1
	#print color_suppressed
	return color_suppressed, borderColor_suppressed, alpha_suppressed
	
def suppressIncreaseDecreaseColors(color, borderColor, values, incDec):
	color_suppressed = np.array(color, copy = True)
	borderColor_suppressed = np.array(borderColor, copy = True)
	alpha = np.full(len(color), 0.85)
	alpha_suppressed = pd.Series(alpha)
	
	for i, d in enumerate(values):
		if d <= 0 and incDec == "Increase":
			color_suppressed[i] = "#f2f2f2"
			borderColor_suppressed[i] = "#bfbfbf"
			alpha_suppressed[i] = 0.1		
		elif d > 0 and incDec == "Decrease":
			color_suppressed[i] = "#f2f2f2"
			borderColor_suppressed[i] = "#bfbfbf"
			alpha_suppressed[i] = 0.1					
	return color_suppressed, borderColor_suppressed, alpha_suppressed
#*****************************
#            LOF
#*****************************
def knn(df, k):
	nbrs = NearestNeighbors(n_neighbors=k)
	nbrs.fit(df)
	distances, indices = nbrs.kneighbors(df)
	return distances, indices
	
def findReachDist(df, kDistance, minPts):
	nbrs = NearestNeighbors(n_neighbors=minPts)
	nbrs.fit(df)
	reachDist, reachIndices = nbrs.kneighbors(df)
	for i in range(reachDist.shape[0]):
		for j in range(1, reachDist.shape[1]):
			reachDist[i, j] = max(reachDist[i, j], kDistance[reachIndices[i, j]])
	return reachDist, reachIndices

def findAvgNeighborLrd(lrd, reachIndices):
	neighborLrds = np.ndarray(shape=(lrd.shape[0]), dtype=float)
	indexToLrd = lambda x: lrd[x]
	for i in range(neighborLrds.shape[0]):
		avg = (np.apply_along_axis(indexToLrd, 0, reachIndices[i,1:])).mean()
		neighborLrds[i] = avg
	return neighborLrds
	
def calcLocalOutlierFactor(inputs, minPts):
	knnDist, knnIndices = knn(inputs, k)
	kDistance = knnDist.max(axis = 1)
	reachDist, reachIndices = findReachDist(inputs, kDistance, minPts)
	lrd = 1 / (reachDist.sum(axis=1) / minPts)
	lof = findAvgNeighborLrd(lrd, reachIndices) / lrd
	return lof

#*****************************
#         Plotting
#*****************************
def plotConnectingLines(ax, x0, x1, y0, y1, z0, z1, alpha):
	for i in range(x0.shape[0]):
		ax.plot(xs=[x0.iloc[i],x1.iloc[i]],
				ys=[y0.iloc[i],y1.iloc[i]],
				zs=[z0.iloc[i],z1.iloc[i]],
				color='black', alpha=alpha.iloc[i])
				
def createTopText(df, title, h1, h2, h2chars, h3, h3chars, h4):
	text = title + "\n"
	text += "  " + h1.ljust(10) + h2.ljust(8 + h2chars) + h3.ljust(8 + h3chars) + h4 + "\n"
	for i, row in df.iterrows():
		fig0 = str(round(row[0], 1))
		if isinstance(row[1], basestring):
			fig1 = row[1]
		else:
			fig1 = str(round(row[1], 1))
		fig2 = str(round(row[2], 1)) if len(row) > 2 else ""
		text += "  " + str(i).ljust(10) + fig0.ljust(8) + fig1.ljust(8) + fig2 + "\n"
	text += "\n"
	return text

def getOutlierBreakdownText(op_join):
	text = ""
	topInputOutliersOverTime = op_join.nlargest(5, columns='lof_changeInputsAvg')[['lof_changeInputsAvg', 'sd_changeOutputAvg', 'score_change']]
	topInputOutliersConstantTime = op_join.nlargest(5, columns='lof_constantInputsAvg')[['lof_constantInputsAvg', 'sd_constantOutputAvg', 'score_constant']]
	topOutputOutliersOverTime = op_join.nlargest(5, columns='sd_changeOutputAvg')[['sd_changeOutputAvg', 'lof_changeInputsAvg', 'score_change']]
	topOutputOutliersConstantTime = op_join.nlargest(5, columns='sd_constantOutputAvg')[['sd_constantOutputAvg', 'lof_constantInputsAvg', 'score_constant']]
	text += createTopText(topInputOutliersOverTime, "OUTLIERS ('LOF' measures outlierishness):\n\nInputs: Change Over Time", "OpId", "LOF", 0, r"$\sigma$(EUR$\Delta$)", 14, "Score")
	text += createTopText(topOutputOutliersOverTime, "Outputs: Change Over Time", "OpId", r"$\sigma$(EUR$\Delta$)", 14, "LOF", 0, "Score")
	text += createTopText(topInputOutliersConstantTime, "Inputs: Constant Over Time", "OpId", "LOF", 0, r"$\sigma$(EUR)", 7, "Score")
	text += createTopText(topOutputOutliersConstantTime, "Outputs: Constant Over Time", "OpId", r"$\sigma$(EUR)", 7, "LOF", 0, "Score")
	return text

def getOutlierScoringText(op_join):
	text = ""
	changeLeaders = op_join.nlargest(5, columns='score_change')[['score_change', 'lof_changeInputsAvg', 'sd_changeOutputAvg']]
	constantLeaders = op_join.nlargest(5, columns='score_constant')[['score_constant', 'lof_constantInputsAvg', 'sd_constantOutputAvg']]
	combinedLeaders = op_join.nlargest(5, columns='score_combined')[['score_combined', 'score_combined_type']]
	text += createTopText(changeLeaders, "TOP SCORERS:\n\nChange Over Time:", "OpId", "Score", 0, "LOF", 0, r"$\sigma$(EUR$\Delta$)")
	text += createTopText(constantLeaders, "Constant Over Time", "OpId", "Score", 0, "LOF", 0, r"$\sigma$(EUR)")
	text += createTopText(combinedLeaders, "Change + Constant", "OpId", "Score", 0, "Type", 0, "")	
	return text

def plotHistogram(fig, sub_i, sub_j, sub_index, values, title):
	ax_hist = fig.add_subplot(sub_i, sub_j, sub_index)
	n, bins, patches = ax_hist.hist(values, 20, normed=1, facecolor='#F0F0F0')
	mu = values.mean()
	sigma = values.std()	
	sigmaHalf = sigma / 2
	y = mlab.normpdf(bins, mu, sigma)
	yHalf = mlab.normpdf(bins, mu, sigmaHalf)
	ax_hist.plot(bins, y, c='red', linewidth=1)
	ax_hist.plot(bins, yHalf, c='purple', linewidth=1)
	ax_hist.axvline(mu + 3 * sigma)
	ax_hist.axvline(mu - 3 * sigma)			
	ax_hist.set_title("Ratio of Change in EUR")
	return ax_hist
	
def plotText(fig, sub_i, sub_j, sub_index, text, font, bottom, color="black"):
	ax_text = fig.add_subplot(sub_i, sub_j, sub_index)
	ax_text.text(0.05, bottom, text, fontdict=font, color=color)
	ax_text.tick_params(labelbottom='off', labelleft='off', top='off', bottom='off', left='off', right='off')
	ax_text.axis('off')
	return ax_text

def plotRegression(fig, sub_i, sub_j, col, x, y, xlab, ylab, x_thousands, y_thousands):
	fmt = FuncFormatter(formatThousands)
	ax_scatter = plotScatter(fig, sub_i, sub_j, col, x, y, xlab, ylab, xlab + " vs " + ylab)
	lm = sm.OLS(y, x).fit()
	ax_scatter.plot(x, lm.fittedvalues, c='black')
	if(x_thousands):
		ax_scatter.xaxis.set_major_formatter(fmt)	
	if(y_thousands):
		ax_scatter.yaxis.set_major_formatter(fmt)
	
	ax_residuals = plotScatter(fig, sub_i, sub_j, col + sub_j, x, (y - lm.fittedvalues), 
					xlab, ylab + " Residuals", "Residuals: " + xlab + " vs " + ylab)
	xMax = x.max() / ax_residuals.get_xlim()[1]
	xMin = (x.min() - ax_residuals.get_xlim()[0]) / ax_residuals.get_xlim()[1]
	ax_residuals.axhline(y=0, c='black')
	if(x_thousands):
		ax_residuals.xaxis.set_major_formatter(fmt)	
	if(y_thousands):
		ax_residuals.yaxis.set_major_formatter(fmt)	
	#print lm.summary()
	return ax_scatter, ax_residuals, lm

def plotScatter(fig, sub_i, sub_j, col, x, y, xlab, ylab, title):
	ax = fig.add_subplot(sub_i, sub_j, col)
	ax.scatter(x, y, c='black', s=4)
	setLabels(ax, xlab, ylab, None, title)
	return ax
	
def plotTopInsights(op_join, fig, sub_i, sub_j, sub_index, font, bottom):
	combinedLeaders = op_join.nlargest(5, columns='score_combined')[
			['score_combined', 'score_combined_type', 'sd_changeOutputAvg', 'avg_eur_x', 'avg_eur_y', 'sd_constantOutputAvg', 'lof_constantInputsAvg']]
	i = 1
	for opId, row in combinedLeaders.iterrows():
		insight = str(i) + ". Op " + str(opId) + " was an outlier"
		if(row[1] == "Change"):
			insight += " in terms of YoY change.\n"
			insight += "They were unusual in terms of " + xlab + ", " + ylab + " and/or " + zlab + ".\n"
			if row[2] > 1.65:
				if row[4] - row[3] > 0:
					insight += "EUR increased by a significant amount"
				else:
					insight += "EUR increased by a significant amount"
			else:
				insight += "EUR did not increase significantly"
		else:
			insight += " in 2013 and 2014.\n"
			outputSignficant = False
			if row[5] > 2.8:
				insight += "They had a significant level of " + dlab + "\n"
				outputSignificant = True
			lofSignificant = False
			if row[6] > 7:
				insight += "They were "
				if(outputSignficant):
					insight += "also "
				insight += "unusual in terms of " + xlab + ", " + ylab + " and/or " + zlab + ".\n"
				lofSignificant
			if(not outputSignficant and not lofSignificant):
				insight += "They had an usual combination of " + dlab + "\nand " + xlab + ", " + ylab + " and/or " + zlab + "."
		color = getInsightRankColor(i)
		plotText(fig, sub_i, sub_j, sub_index, insight, font, 1.1 - (0.2 * i), color)
		i += 1

def formatThousands(x, pos):
	return '%1.fK' % (x * 1e-3)

def plotDimensionalMovers(fig, sub_i, sub_j, col, x0, x1, y0, y1, z0, z1, s0, s1, color_pos, 
							borderColor_pos, alpha_pos, color_neg, borderColor_neg, alpha_neg, 
							xlab, ylab, zlab, lab, x_thousands, y_thousands, z_thousands):
	ax_pos = fig.add_subplot(sub_i, sub_j, col, projection='3d')
	ax_pos.scatter(x0, y0, z0, c=color_pos, color=borderColor_pos, marker='o', s=s0)
	ax_pos.scatter(x1, y1, z1, c=color_pos, color=borderColor_pos, marker='o', s=s1)
	setLabels(ax_pos, xlab, ylab, zlab, "Increase in " + lab, x_thousands, y_thousands, z_thousands)
	setAxesLimits(ax_pos, None, None, None, invertZ)
	plotConnectingLines(ax_pos, x0, x1, y0, y1, z0, z1, alpha_pos)
	
	ax_neg = fig.add_subplot(sub_i, sub_j, col + sub_j, projection='3d')
	ax_neg.scatter(x0, y0, z0, c=color_neg, color=borderColor_neg, marker='o', s=s0)
	ax_neg.scatter(x1, y1, z1, c=color_neg, color=borderColor_neg, marker='o', s=s1)
	setLabels(ax_neg, xlab, ylab, zlab, "Decrease in " + lab, x_thousands, y_thousands, z_thousands)
	setAxesLimits(ax_neg, None, None, None, invertZ)
	plotConnectingLines(ax_neg, x0, x1, y0, y1, z0, z1, alpha_neg)
	
	return ax_pos, ax_neg

def calcTTestScores(p1, p2, dim, type):
	avg_p1 = p1.mean()
	avg_p2 = p2.mean()
	count_p1 = p1.shape[0]
	count_p2 = p2.shape[0]
	var_p1 = p1.var()
	var_p2 = p2.var()
	
	tScore = (avg_p1 - avg_p2) / np.sqrt((var_p1 / count_p1) + (var_p2 / count_p2))
	df = int((((var_p1 / count_p1) + (var_p2 / count_p2))**2) / (((var_p1 / count_p1)**2 / (count_p1 - 1)) + ((var_p2 / count_p2)**2 / (count_p2 - 1))))
	pValue = stats.t.sf(np.abs(tScore), df)*2
	
	return {'type' : type, 'dimension' : dim, 'tScore' : tScore, 'pValue' : pValue, 'df' : df}

def calcMovementTScores(op_join_pos, op_join_neg, op_join_pos_negation, op_join_neg_negation, dim, d_id):
	# d_id = dim['id']
	d_pos = op_join_pos[d_id + "_y"] - op_join_pos[d_id + "_x"]
	d_neg = op_join_neg[d_id + "_y"] - op_join_neg[d_id + "_x"]
	d_pos_negation = op_join_pos_negation[d_id + "_y"] - op_join_pos_negation[d_id + "_x"]
	d_neg_negation = op_join_neg_negation[d_id + "_y"] - op_join_neg_negation[d_id + "_x"]
	
	positiveVsNegation = calcTTestScores(d_pos, d_pos_negation, dim, 'positive-negation')
	positiveVsNeg = calcTTestScores(d_pos, d_neg, dim, 'positive-neg')
	negVsNegation = calcTTestScores(d_neg, d_neg_negation, dim, 'neg-negation')
	
	return [positiveVsNegation, positiveVsNeg, negVsNegation]
	
def calcIncreaseDecreaseTScores(op_join_inc, op_join_dec, dim):
	d_id = dim['id']
	values_inc = op_join_inc[d_id + "_y"] - op_join_inc[d_id + "_x"]
	values_dec = op_join_dec[d_id + "_y"] - op_join_dec[d_id + "_x"]
	
	increaseVsDecrease = calcTTestScores(values_inc, values_dec, dim, 'increase-decrease')
	return [increaseVsDecrease]

def createMovementText(m, dlab):
	text = ""
	dim = m['dimension']['name']
	type = m['type']
	if(type == 'increase-decrease'):
		indIncDec = "increased" if m['tScore'] > 0 else "decreased"
		text = "Operators who increased EUR generally " + indIncDec + " " + dim + " as well"
	else:
		depIncDec = "increase" if m['tScore'] > 0 else "decrease"
		if(type == 'positive-negation'):
			text = "Operators that increased " + dim + " tended to " + depIncDec + " " + dlab
		elif(type == 'positive-neg'):
			text = "Operators that increased " + dim + " tended to " + depIncDec + " " + dlab + " while the opposite was true for those that decreased " + dim
		elif(type == 'neg-negation'):
			text = "Operators that decreased " + dim + " tended to " + depIncDec + " " + dlab 
	return text
	
###########################
#    Set Up Variables
###########################

k = 4
minPts = 10
SUB_I = 2 # Subplot rows
SUB_J = 4 # Subplot cols
FONT = {'family': 'monospace',
		'weight': 'normal',
		'size': 10,
		}	
FONT_MED = {'family': 'monospace',
	'weight': 'normal',
	'size': 14,
	}
plotType = "Movers" # Outliers, Movers, Regression
moveBasedOnSd = True
moveSd = 0.5
insightColoring = True
times = []
formations = [{'id':19, 'name':'Eagle Ford'}, {'id':70, 'name':'Wolfcamp'}, {'id':7, 'name':'Bakken'}, {'id':53, 'name':'Permian'}]
variables = {'eur':{'id':'avg_eur', 'name':'EUR','invertZ':False,'thousands':True}, 
				'water':{'id':'avg_water_vol_kgal', 'name':'Water','invertZ':False,'thousands':True}, 
				'proppant':{'id':'avg_proppant_lbs', 'name':'Proppant','invertZ':False,'thousands':True}, 
				'vertDepth':{'id':'avg_vert_depth', 'name':'Vertical Depth','invertZ':True,'thousands':True},
				'ppg':{'id':'avg_ppg', 'name':'PPG','invertZ':False,'thousands':False},
				'perfLength':{'id':'avg_perf_length', 'name':'Perf Length','invertZ':False,'thousands':True},
				'measureDepth':{'id':'avg_measure_depth', 'name':'Measure Depth','invertZ':True,'thousands':True},
				'ppf':{'id':'ppf', 'name':'PPF','invertZ':False,'thousands':False},
				'wellCount':{'id':'well_count', 'name':'Well Count','invertZ':False,'thousands':False},
				'surfSpacing':{'id':'avg_surf_spacing', 'name':'Surface Spacing Ft', 'invertZ':False,'thousands':False},
				'timeToHalfEUR':{'id':'avg_time_to_half_eur', 'name':'Time to Half EUR', 'invertZ':False,'thousands':False}
				}
dimensionList = [{'x':variables['water'], 'y':variables['timeToHalfEUR'], 'z':variables['measureDepth'], 'd':variables['eur']}]
				#{'x':variables['ppg'], 'y':variables['perfLength'], 'z':variables['measureDepth']}]#, 
				#{'x':variables['water'], 'y':variables['proppant'], 'z':variables['vertDepth']}]

# SQL
db = MySQLdb.connect(read_default_file="D:/Angus/Workspace/.my.cnf", read_default_group='mysql') 

# Loop through formations
for formation in formations:
	###########################
	#       Format Data
	###########################
	t0_sql = time.clock()
	df = pd.read_sql("call analysis.ap_cluster_get_pad_properties(" + str(formation['id']) + ", '20130101', '20160101');", con=db)
	t1_sql = time.clock()
	times.append({'process' : 'SQL', 'time' : t1_sql - t0_sql})
	
	t0_data = time.clock()
	df_2013 = df[df['avg_spud_dt'].dt.year == 2013]
	df_2014 = df[df['avg_spud_dt'].dt.year == 2014]
	df_2015 = df[df['avg_spud_dt'].dt.year == 2015]
	df_2013_dropna = cleanDataFrame(df_2013)
	df_2014_dropna = cleanDataFrame(df_2014)
	op_2013 = groupByOperator(df_2013_dropna)
	op_2014 = groupByOperator(df_2014_dropna)
	op_join = pd.merge(op_2013, op_2014, left_index=True, right_index=True)
	
	maxEUR = np.maximum(op_join['avg_eur_x'].max(), op_join['avg_eur_y'].max())
	t1_data = time.clock()
	times.append({'process' : 'Rearrange Data', 'time' : t1_data - t0_data})
	
	for didx, dimensions in enumerate(dimensionList):
		###########################
		#  Set Dimension Variables
		###########################
		t0_vars = time.clock()
		invertZ = dimensions['z']['invertZ']
		xlab = dimensions['x']['name']
		ylab = dimensions['y']['name']
		zlab = dimensions['z']['name']
		dlab = dimensions['d']['name']
		x_thousands = dimensions['x']['thousands']
		y_thousands = dimensions['y']['thousands']
		z_thousands = dimensions['z']['thousands']
		d_thousands = dimensions['d']['thousands']
		
		x0 = op_join[dimensions['x']['id'] + "_x"]
		y0 = op_join[dimensions['y']['id'] + "_x"]
		z0 = op_join[dimensions['z']['id'] + "_x"]
		d0 = op_join[dimensions['d']['id'] + "_x"]
		
		x1 = op_join[dimensions['x']['id'] + "_y"]
		y1 = op_join[dimensions['y']['id'] + "_y"]
		z1 = op_join[dimensions['z']['id'] + "_y"]
		d1 = op_join[dimensions['d']['id'] + "_y"]
		
		x_delta = x1 - x0
		y_delta = y1 - y0
		z_delta = z1 - z0
		d_delta = d1 - d0

		op_join['x_pctDiff'] = np.where(x0 != 0, (x1 - x0) / x0, np.where(x1 != 0, 1000 * (x1 / x1.abs()), 0))
		op_join['y_pctDiff'] = np.where(y0 != 0, (y1 - y0) / y0, np.where(y1 != 0, 1000 * (y1 / y1.abs()), 0))
		op_join['z_pctDiff'] = np.where(z0 != 0, (z1 - z0) / z0, np.where(z1 != 0, 1000 * (z1 / z1.abs()), 0))
		op_join['d_pctDiff'] = np.where(d0 != 0, (d1 - d0) / d0, np.where(d1 != 0, 1000 * (d1 / d1.abs()), 0))
		x_pctDiff = op_join['x_pctDiff']
		y_pctDiff = op_join['y_pctDiff']
		z_pctDiff = op_join['z_pctDiff']
		d_pctDiff = op_join['d_pctDiff']
		
		op_join['x_logRatio'] = np.where((x0 != 0) & (x1 != 0), np.log(x1 / x0), np.where(x1 != 0, np.log(1000 * (x1 / x1.abs())), 0))
		op_join['y_logRatio'] = np.where((y0 != 0) & (y1 != 0), np.log(y1 / y0), np.where(y1 != 0, np.log(1000 * (y1 / y1.abs())), 0))
		op_join['z_logRatio'] = np.where((z0 != 0) & (z1 != 0), np.log(z1 / z0), np.where(z1 != 0, np.log(1000 * (z1 / z1.abs())), 0))
		op_join['d_logRatio'] = np.where((d0 != 0) & (d1 != 0), np.log(d1 / d0), np.where(d1 != 0, np.log(1000 * (d1 / d1.abs())), 0))
		x_logRatio = op_join['x_logRatio']
		y_logRatio = op_join['y_logRatio']
		z_logRatio = op_join['z_logRatio']
		d_logRatio = op_join['d_logRatio']		
		
		op_join['d0_log'] = np.where(op_join['avg_eur_x'] != 0, op_join['avg_eur_x'].apply(np.log), np.log(0.1))
		op_join['d1_log'] = np.where(op_join['avg_eur_y'] != 0, op_join['avg_eur_y'].apply(np.log), np.log(0.1))
		d0_log = op_join['d0_log']
		d1_log = op_join['d1_log']		
		
		t1_vars = time.clock()
		times.append({'process' : 'Set up variables', 'time' : t1_vars - t0_vars})

		###########################
		#        Outliers
		###########################
		
		# Calc Outliers
		t0_out = time.clock()
		lof_t0 = calcLocalOutlierFactor(pd.concat([pd.concat([x0, y0], axis = 1), z0], axis = 1), minPts)
		lof_t1 = calcLocalOutlierFactor(pd.concat([pd.concat([x1, y1], axis = 1), z1], axis = 1), minPts)
		lof_delta = calcLocalOutlierFactor(pd.concat([pd.concat([x_delta, y_delta], axis = 1), z_delta], axis = 1), minPts)
		lof_pctDiff = calcLocalOutlierFactor(pd.concat([pd.concat([x_pctDiff, y_pctDiff], axis = 1), z_pctDiff], axis = 1), minPts)	
		lof_logRatio = calcLocalOutlierFactor(pd.concat([pd.concat([x_logRatio, y_logRatio], axis = 1), z_logRatio], axis = 1), minPts)
		t1_out = time.clock()
		times.append({'process' : 'Calc outliers', 'time' : t1_out - t0_out})
		
		# Score Calculations: Change
		t0_scores = time.clock()
		op_join['lof_delta'] = lof_delta
		op_join['lof_logRatio'] = lof_logRatio
		op_join['lof_changeInputsAvg'] = np.sqrt(lof_delta * lof_logRatio)
		
		op_join['sd_deltaOutput'] = (d_delta - d_delta.mean()) / d_delta.std()
		op_join['sd_logRatioOutput'] = np.array((d_logRatio - d_logRatio.mean()) / d_logRatio.std())
		op_join['sd_changeOutputAvg'] = (op_join['sd_deltaOutput'] * op_join['sd_logRatioOutput']).apply(np.sqrt)

		# Score Calculations: Constant
		op_join['lof_t0'] = lof_t0
		op_join['lof_t1'] = lof_t1
		op_join['lof_constantInputsAvg'] = np.sqrt(lof_t0 * lof_t1)

		cancelNegatives = lambda x: 0.1 if x <= 0 else x
		op_join['sd_logOutput_t0'] = ((d0_log - d0_log.mean()) / d0_log.std()).apply(cancelNegatives)
		op_join['sd_logOutput_t1'] = ((d1_log - d1_log.mean()) / d1_log.std()).apply(cancelNegatives)
		op_join['sd_constantOutputAvg'] = (op_join['sd_logOutput_t0'] * op_join['sd_logOutput_t1']).apply(np.sqrt)
		
		# Score Calculations: Final Scores
		op_join['score_change'] = (op_join['lof_changeInputsAvg'] * op_join['sd_changeOutputAvg']).apply(np.sqrt)
		op_join['score_constant'] = (op_join['lof_constantInputsAvg'] * op_join['sd_constantOutputAvg']).apply(np.sqrt)
		op_join['score_combined'] = op_join[['score_change', 'score_constant']].max(axis=1)
		op_join['score_combined_type'] = np.where(op_join['score_change'] >= op_join['score_constant'], 'Change', 'Constant')
		t1_scores = time.clock()
		times.append({'process' : 'Calc scores', 'time' : t1_scores - t0_scores})
		
		###########################
		#    Coloring / Sizing
		###########################
		t0_plot = time.clock()
		# Coloring by Value
		s0 = d0 / maxEUR * 500	
		s1 = d1 / maxEUR * 500
		color_t0 = '#EEEEEE'
		borderColor_t0 = 'gray'

		color_delta, borderColor_delta, s_delta = getNormalizedRGColorScale(d_delta)
		color_pctDiff, borderColor_pctDiff, s_pctDiff = getNormalizedRGColorScale(d_pctDiff)
		color_logRatio, borderColor_logRatio, s_logRatio = getNormalizedRGColorScale(d_logRatio)
		
		color_t1 = color_delta
		color_ts = color_delta
		borderColor_t1 = borderColor_delta
		borderColor_ts = borderColor_delta		
		magnitude = np.sqrt(x0*x1 + y0*y1 + z0*z1)
		alpha_magnitude = getAlphaScale(magnitude, magnitude.min(), magnitude.max())
		
		# Coloring by Outlier
		outlierSuffix =  "(Blue is 'Outlierishness')"
		if insightColoring:
			scoreRank = op_join['score_combined'].rank(ascending=False)
			color_t0_out, borderColor_t0_out = getInsightColorScale(scoreRank, op_join['score_combined_type'], "Constant")
			color_t1_out, borderColor_t1_out = getInsightColorScale(scoreRank, op_join['score_combined_type'], "Constant")
			color_delta_out, borderColor_delta_out = getInsightColorScale(scoreRank, op_join['score_combined_type'], "Change")		
			color_pctDiff_out, borderColor_pctDiff_out = getInsightColorScale(scoreRank, op_join['score_combined_type'], "Change")
			color_logRatio_out, borderColor_logRatio_out = getInsightColorScale(scoreRank, op_join['score_combined_type'], "Change")
			
			alpha_magnitude_out = getInsightAlphaScale(scoreRank, op_join['score_combined_type'], "Constant")	
			outlierSuffix = ""			
		else:
			lof_t0t1_min = min(lof_t0.min(), lof_t1.min())
			lof_t0t1_max = max(lof_t0.max(), lof_t1.max())
			
			color_t0_out, borderColor_t0_out = getOutlierColorScale(lof_t0, lof_t0t1_min, lof_t0t1_max)
			color_t1_out, borderColor_t1_out = getOutlierColorScale(lof_t1, lof_t0t1_min, lof_t0t1_max)
			color_delta_out, borderColor_delta_out = getOutlierColorScale(lof_delta, lof_delta.min(), lof_delta.max())		
			color_pctDiff_out, borderColor_pctDiff_out = getOutlierColorScale(lof_pctDiff, lof_pctDiff.min(), lof_pctDiff.max())
			color_logRatio_out, borderColor_logRatio_out = getOutlierColorScale(lof_logRatio, lof_logRatio.min(), lof_logRatio.max())
			
			alpha_magnitude_out = getAlphaScale((lof_t1 + lof_t0) / 2, lof_t0t1_min, lof_t0t1_max)	
		
		# Coloring by Movement
		color_x_pos, borderColor_x_pos, alpha_x_pos = suppressMovementColors(color_delta, borderColor_delta, 
														x_delta, y_delta, z_delta, "x", "positive", moveBasedOnSd, moveSd)
		color_x_neg, borderColor_x_neg, alpha_x_neg = suppressMovementColors(color_delta, borderColor_delta, 
														x_delta, y_delta, z_delta, "x", "negative", moveBasedOnSd, moveSd)
		color_y_pos, borderColor_y_pos, alpha_y_pos = suppressMovementColors(color_delta, borderColor_delta, 
														x_delta, y_delta, z_delta, "y", "positive", moveBasedOnSd, moveSd)
		color_y_neg, borderColor_y_neg, alpha_y_neg = suppressMovementColors(color_delta, borderColor_delta, 
														x_delta, y_delta, z_delta, "y", "negative", moveBasedOnSd, moveSd)
		color_z_pos, borderColor_z_pos, alpha_z_pos = suppressMovementColors(color_delta, borderColor_delta, 
														x_delta, y_delta, z_delta, "z", "positive", moveBasedOnSd, moveSd)
		color_z_neg, borderColor_z_neg, alpha_z_neg = suppressMovementColors(color_delta, borderColor_delta, 
														x_delta, y_delta, z_delta, "z", "negative", moveBasedOnSd, moveSd)
		color_inc, borderColor_inc, alpha_inc = suppressIncreaseDecreaseColors(color_delta, borderColor_delta, d_delta, "Increase")
		color_dec, borderColor_dec, alpha_dec = suppressIncreaseDecreaseColors(color_delta, borderColor_delta, d_delta, "Decrease")
		
		###########################
		#          Plot
		###########################
		fig = plot.figure()
		fig.suptitle(xlab + " x " + ylab + " x " + zlab + ": " + dlab + " (" + formation['name'] + ", 2013-2014)", fontsize=20)

		if plotType == "Outliers":
			# # t0
			# ax_t0 = fig.add_subplot(SUB_I, SUB_J, 1, projection='3d')
			# ax_t0.scatter(x0, y0, z0, c=color_t0, color=borderColor_t0,  marker='o', s=s0)
			# setLabels(ax_t0, xlab, ylab, zlab, "2013")

			# # t1
			# ax_t1 = fig.add_subplot(SUB_I, SUB_J, 2, projection='3d')
			# ax_t1.scatter(x1, y1, z1, c=color_t1, color=borderColor_t1, marker='o', s=s1)
			# setLabels(ax_t1, xlab, ylab, zlab, "2014 (Green/Red is +/- in EUR)")

			# t0 -> t1
			ax_ts = fig.add_subplot(SUB_I, SUB_J, 1, projection='3d')
			ax_ts.scatter(x0, y0, z0, c=color_t0, color=borderColor_t0, marker='o', s=s0)
			ax_ts.scatter(x1, y1, z1, c=color_t1, color=borderColor_t1, marker='o', s=s1)
			setLabels(ax_ts, xlab, ylab, zlab, "2013 and 2014 (R/G is EUR +/-)")
			setAxesLimits(ax_ts, None, None, None, invertZ)
			plotConnectingLines(ax_ts, x0, x1, y0, y1, z0, z1, alpha_magnitude)
						
			ax_ts_out = fig.add_subplot(SUB_I, SUB_J, 5, projection='3d')
			ax_ts_out.scatter(x0, y0, z0, c=color_t0_out, color=borderColor_t0_out, marker='o', s=s0)
			ax_ts_out.scatter(x1, y1, z1, c=color_t1_out, color=borderColor_t1_out, marker='o', s=s1)
			setLabels(ax_ts_out, xlab, ylab, zlab, "2013 and 2014" + outlierSuffix)
			setAxesLimits(ax_ts_out, None, None, None, invertZ)
			plotConnectingLines(ax_ts_out, x0, x1, y0, y1, z0, z1, alpha_magnitude_out)
			
			# # Set axes of t0 and t1 equal to (t1-t0) axes
			# ylim = ax_ts.get_ylim()
			# xlim = ax_ts.get_xlim()
			# zlim = ax_ts.get_zlim()
			# setAxesLimits(ax_t0, xlim, ylim, zlim, invertZ)
			# setAxesLimits(ax_t1, xlim, ylim, zlim, invertZ)
			# setAxesLimits(ax_ts, None, None, None, invertZ)
			
			# t1 - t0
			ax_delta = fig.add_subplot(SUB_I, SUB_J, 2, projection='3d')
			ax_delta.scatter(x_delta, y_delta, z_delta, c=color_delta, color=borderColor_delta, marker='o', s=s_delta)
			setLabels(ax_delta, 'Change:' + xlab, 'Change: ' + ylab, 'Change: ' + zlab, "YoY Diff. (R/G is EUR +/-)")
			setAxesLimits(ax_delta, None, None, None, invertZ)

			ax_delta_out = fig.add_subplot(SUB_I, SUB_J, 6, projection='3d')
			ax_delta_out.scatter(x_delta, y_delta, z_delta, c=color_delta_out, color=borderColor_delta_out, marker='o', s=s_delta)
			setLabels(ax_delta_out, 'Change:' + xlab, 'Change: ' + ylab, 'Change: ' + zlab, "YoY Diff." + outlierSuffix)
			setAxesLimits(ax_delta_out, None, None, None, invertZ)		
				
			# # (t1 - t0) / t0
			# ax_pctDiff = fig.add_subplot(SUB_I, SUB_J, 6, projection='3d')
			# ax_pctDiff.scatter(x_pctDiff, y_pctDiff, z_pctDiff, c=color_pctDiff, color=borderColor_pctDiff, marker='o', s=s_pctDiff)
			# setLabels(ax_pctDiff, '% Change: ' + xlab, '% Change: ' + ylab, '% Change: ' + zlab, "% Change from 2013 to 2014")
			# setAxesLimits(ax_pctDiff, None, None, None, invertZ)
			
			# log(t1 / t0)
			ax_logRatio = fig.add_subplot(SUB_I, SUB_J, 3, projection='3d')
			ax_logRatio.scatter(x_logRatio, y_logRatio, z_logRatio, c=color_logRatio, color=borderColor_logRatio, marker='o', s=s_logRatio)
			setLabels(ax_logRatio, 'Log Ratio: ' + xlab, 'Log Ratio: ' + ylab, 'Log Ratio: ' + zlab, "Log Ratio (R/G is EUR Ratio)")
			setAxesLimits(ax_logRatio, None, None, None, invertZ)		

			ax_logRatio_out = fig.add_subplot(SUB_I, SUB_J, 7, projection='3d')
			ax_logRatio_out.scatter(x_logRatio, y_logRatio, z_logRatio, c=color_logRatio_out, color=borderColor_logRatio_out, marker='o', s=s_logRatio)
			setLabels(ax_logRatio_out, 'Log Ratio: ' + xlab, 'Log Ratio: ' + ylab, 'Log Ratio: ' + zlab, "Log Ratio" + outlierSuffix)
			setAxesLimits(ax_logRatio_out, None, None, None, invertZ)	
			
			# Histograms
			# ax_hist_logRatio = plotHistogram(fig, SUB_I, SUB_J, 8, d0, "EUR 2013")
			# ax_hist_logRatio = plotHistogram(fig, SUB_I, SUB_J, 8, d1, "EUR 2014")
			# ax_hist_logRatio = plotHistogram(fig, SUB_I, SUB_J, 8, d_delta, "Delta EUR")
			# ax_hist_logRatio = plotHistogram(fig, SUB_I, SUB_J, 8, d_logRatio, "Ratio of Change in EUR")
			# ax_hist_logRatio = plotHistogram(fig, SUB_I, SUB_J, 8, d0_log, "Log(EUR 2013)")
			
			# Text		
			# outlierBreakdown = getOutlierBreakdownText(op_join)
			# ax_outlierBreakdown = plotText(fig, SUB_I, SUB_J, 4, outlierBreakdown, FONT, -0.1)

			outlierScoring = getOutlierScoringText(op_join)
			ax_outlierScoring = plotText(fig, SUB_I, SUB_J, 4, outlierScoring, FONT, 0.05)
			
			plotTopInsights(op_join, fig, SUB_I, SUB_J, 8, FONT, 0.05)
		elif plotType == "Movers":
			op_join['x_delta_norm'] = getColumnNorm(x_delta)
			op_join['y_delta_norm'] = getColumnNorm(y_delta)
			op_join['z_delta_norm'] = getColumnNorm(z_delta)
			op_join['d_delta_norm'] = getColumnNorm(d_delta)
			
			x_pos = op_join[(op_join['x_delta_norm'] > 0) & (op_join['x_delta_norm'].abs() > moveSd)]
			x_neg = op_join[(op_join['x_delta_norm'] <= 0) & (op_join['x_delta_norm'].abs() > moveSd)]
			x_pos_negation = op_join[(op_join['x_delta_norm'] <= 0) | (op_join['x_delta_norm'].abs() <= moveSd)]
			x_neg_negation = op_join[(op_join['x_delta_norm'] > 0) | (op_join['x_delta_norm'].abs() <= moveSd)]
			y_pos = op_join[(op_join['y_delta_norm'] > 0) & (op_join['y_delta_norm'].abs() > moveSd)]
			y_neg = op_join[(op_join['y_delta_norm'] <= 0) & (op_join['y_delta_norm'].abs() > moveSd)]
			y_pos_negation = op_join[(op_join['y_delta_norm'] <= 0) | (op_join['y_delta_norm'].abs() <= moveSd)]
			y_neg_negation = op_join[(op_join['y_delta_norm'] > 0) | (op_join['y_delta_norm'].abs() <= moveSd)]			
			z_pos = op_join[(op_join['z_delta_norm'] > 0) & (op_join['z_delta_norm'].abs() > moveSd)]
			z_neg = op_join[(op_join['z_delta_norm'] <= 0) & (op_join['z_delta_norm'].abs() > moveSd)]
			z_pos_negation = op_join[(op_join['z_delta_norm'] <= 0) | (op_join['z_delta_norm'].abs() <= moveSd)]
			z_neg_negation = op_join[(op_join['z_delta_norm'] > 0) | (op_join['z_delta_norm'].abs() <= moveSd)]

			movementTests = []
			movementTests.extend(calcMovementTScores(x_pos, x_neg, x_pos_negation, x_neg_negation, dimensions['x'], dimensions['d']['id']))
			movementTests.extend(calcMovementTScores(y_pos, y_neg, y_pos_negation, y_neg_negation, dimensions['y'], dimensions['d']['id']))
			movementTests.extend(calcMovementTScores(z_pos, z_neg, z_pos_negation, z_neg_negation, dimensions['z'], dimensions['d']['id']))
			
			rankedMovements = sorted(movementTests, key = lambda x: x['pValue'])
			
			increaseDecreaseTests = []			
			op_join_inc = op_join[d_delta > 0]
			op_join_dec = op_join[d_delta <= 0]
			increaseDecreaseTests.extend(calcIncreaseDecreaseTScores(op_join_inc, op_join_dec, dimensions['x']))
			increaseDecreaseTests.extend(calcIncreaseDecreaseTScores(op_join_inc, op_join_dec, dimensions['y']))
			increaseDecreaseTests.extend(calcIncreaseDecreaseTScores(op_join_inc, op_join_dec, dimensions['z']))
			rankedIncreaseDecrease = sorted(increaseDecreaseTests, key = lambda x: x['pValue'])

			combinedScores = increaseDecreaseTests + movementTests
			combinedScoresRanked = sorted(combinedScores, key = lambda x: x['pValue'])
				
			ax_pos_x, ax_neg_x = plotDimensionalMovers(fig, SUB_I, SUB_J, 1, x0, x1, y0, y1, z0, z1, s0, s1, color_x_pos, 
				borderColor_x_pos, alpha_x_pos, color_x_neg, borderColor_x_neg, alpha_x_neg, 
				xlab, ylab, zlab, xlab, x_thousands, y_thousands, z_thousands)
			
			ax_pos_y, ax_neg_y = plotDimensionalMovers(fig, SUB_I, SUB_J, 2, x0, x1, y0, y1, z0, z1, s0, s1, color_y_pos, 
				borderColor_y_pos, alpha_y_pos, color_y_neg, borderColor_y_neg, alpha_y_neg, 
				xlab, ylab, zlab, ylab, x_thousands, y_thousands, z_thousands)

			ax_pos_z, ax_neg_z = plotDimensionalMovers(fig, SUB_I, SUB_J, 3, x0, x1, y0, y1, z0, z1, s0, s1, color_z_pos, 
				borderColor_z_pos, alpha_z_pos, color_z_neg, borderColor_z_neg, alpha_z_neg, 
				xlab, ylab, zlab, zlab, x_thousands, y_thousands, z_thousands)

			ax_inc = fig.add_subplot(SUB_I, SUB_J, 4, projection='3d')
			ax_inc.scatter(x0, y0, z0, c=color_t0, color=borderColor_t0, marker='o', s=s0)
			ax_inc.scatter(x1, y1, z1, c=color_inc, color=borderColor_inc, marker='o', s=s1)
			setLabels(ax_inc, xlab, ylab, zlab, "Increase in " + dlab, x_thousands, y_thousands, z_thousands)
			setAxesLimits(ax_inc, None, None, None, invertZ)
			plotConnectingLines(ax_inc, x0, x1, y0, y1, z0, z1, alpha_inc)
						
			ax_dec = fig.add_subplot(SUB_I, SUB_J, 8, projection='3d')
			ax_dec.scatter(x0, y0, z0, c=color_t0, color=borderColor_t0, marker='o', s=s0)
			ax_dec.scatter(x1, y1, z1, c=color_dec, color=borderColor_dec, marker='o', s=s1)
			setLabels(ax_dec, xlab, ylab, zlab, "Decrease in " + dlab, x_thousands, y_thousands, z_thousands)
			setAxesLimits(ax_dec, None, None, None, invertZ)
			plotConnectingLines(ax_dec, x0, x1, y0, y1, z0, z1, alpha_dec)

			text = ""
			for i, score in enumerate(combinedScoresRanked[:4]):
				text += str(i + 1) + ". " + createMovementText(score, dlab) + "\n"#(score['type'] + " for " + score['dimension']['name']).ljust(60) + " pvalue = " + str(round(score['pValue'], 3)) + "\n"
			plot.figtext(0.27, 0.015, text, fontdict=FONT, color="black")

 
		elif plotType == "Regression":
			# ax_x0, ax_x0_res, lm_x0 = plotRegression(fig, SUB_I, SUB_J, 1, x0, d0, xlab, dlab, dimensions['x']['thousands'], dimensions['d']['thousands'])
			# ax_y0, ax_y0_res, lm_y0 = plotRegression(fig, SUB_I, SUB_J, 2, y0, d0, ylab, dlab, dimensions['y']['thousands'], dimensions['d']['thousands'])
			# ax_z0, ax_z0_res, lm_z0 = plotRegression(fig, SUB_I, SUB_J, 3, z0, d0, zlab, dlab, dimensions['z']['thousands'], dimensions['d']['thousands'])
			
			# ax_x1, ax_x1_res, lm_x1 = plotRegression(fig, SUB_I, SUB_J, 1, x1, d1, xlab, dlab, dimensions['x']['thousands'], dimensions['d']['thousands'])
			# ax_y1, ax_y1_res, lm_y1 = plotRegression(fig, SUB_I, SUB_J, 2, y1, d1, ylab, dlab, dimensions['y']['thousands'], dimensions['d']['thousands'])
			# ax_z1, ax_z1_res, lm_z1 = plotRegression(fig, SUB_I, SUB_J, 3, z1, d1, zlab, dlab, dimensions['z']['thousands'], dimensions['d']['thousands'])
			
			ax_x_delta, ax_x_delta_res, lm_x_delta = plotRegression(fig, SUB_I, SUB_J, 1, x_delta, d_delta, r'$\Delta$' + xlab, r'$\Delta$' + dlab, x_thousands, d_thousands)
			ax_y_delta, ax_y_delta_res, lm_y_delta = plotRegression(fig, SUB_I, SUB_J, 2, y_delta, d_delta, r'$\Delta$' + ylab, r'$\Delta$' + dlab, y_thousands, d_thousands)
			ax_z_delta, ax_z_delta_res, lm_z_delta = plotRegression(fig, SUB_I, SUB_J, 3, z_delta, d_delta, r'$\Delta$' + zlab, r'$\Delta$' + dlab, z_thousands, d_thousands)

			# ax_x_logRatio, ax_x_logRatio_res, lm_x_logRatio = plotRegression(fig, SUB_I, SUB_J, 1, x_logRatio, d_logRatio, 'Log-Ratio(' + xlab + ')', 'Log-Ratio(' + dlab + ')', False, False)
			# ax_y_logRatio, ax_y_logRatio_res, lm_y_logRatio = plotRegression(fig, SUB_I, SUB_J, 2, y_logRatio, d_logRatio, 'Log-Ratio(' + ylab + ')', 'Log-Ratio(' + dlab + ')', False, False)
			# ax_z_logRatio, ax_z_logRatio_res, lm_z_logRatio = plotRegression(fig, SUB_I, SUB_J, 3, z_logRatio, d_logRatio, 'Log-Ratio(' + zlab + ')', 'Log-Ratio(' + dlab + ')', False, False)
			
			# print lm_x0.summary()
			# print lm_x0.params
			# print lm_x0.params[0]
			lm_x = lm_x_delta
			lm_y = lm_y_delta
			lm_z = lm_z_delta
			
			regressionSummary = ""
			for lab, lm in zip([xlab, ylab, zlab], [lm_x, lm_y, lm_z]):
				regressionSummary += lab + "\n"
				regressionSummary += " r-squared: ".ljust(18) + str(round(lm.rsquared, 2)) + "\n"
				regressionSummary += " adj r-squared: ".ljust(18) + str(round(lm.rsquared_adj, 2)) + "\n"
				regressionSummary += " coefficient: ".ljust(18) + str(round(lm.params[0], 2)) + "\n"
				regressionSummary += " p-value: ".ljust(18) + str(round(lm.pvalues[0], 4)) + "\n"
				regressionSummary += "\n"
				print lm.summary()
			ax_summary = plotText(fig, SUB_I, SUB_J, 4, regressionSummary, FONT_MED, 0.02)
			
			# color_3d, borderColor_3d = getGreenColorScale(d1, d1.min(), d1.max())
			ax_3d = fig.add_subplot(SUB_I, SUB_J, 8, projection='3d')
			ax_3d.scatter(x_delta, y_delta, z_delta, c=color_delta, color=borderColor_delta,  marker='o', s=s_delta)
			setLabels(ax_3d, xlab, ylab, zlab, "Diff. in EUR (R/G is +/- EUR)", x_thousands, y_thousands, z_thousands)
			setAxesLimits(ax_3d, None, None, None, invertZ)
		t1_plot = time.clock()
		times.append({'process' : 'Plot', 'time' : t1_plot - t0_plot})
		
plot.show()	
db.close()

totalTimes = {}
for timing in times:
	if(timing['process'] in totalTimes):
		totalTimes[timing['process']]['n'] += 1
		totalTimes[timing['process']]['time'] += timing['time']
	else:
		dict = {'process': timing['process'], 'time': timing['time'], 'n' : 1}
		totalTimes[timing['process']] = dict
print totalTimes
for process, dict in totalTimes.iteritems():
	avgTime = dict['time'] / dict['n']
	print (process + ":").ljust(18) + str(avgTime * 1000) + " ms"